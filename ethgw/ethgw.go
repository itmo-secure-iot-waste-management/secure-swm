package main

import (
    "log"
    "os"
    "strings"
    "strconv"
    "net/http"
    "encoding/json"
    "io/ioutil"
    "math/big"
    "time"
    "fmt"
    "bytes"
    "flag"
    "sync"
    
    "github.com/onrik/ethrpc"
    . "github.com/logrusorgru/aurora"
)

// global variable holding the serving PORT for this GW's API
var API_PORT string

// define operating mode via CLI arg parsed at startup
var GW_OP_MODE string

// enpoints for ETH RPC Node and Quantum BCNode
var ETH_NODE_ADDRESS string
var QNET_NODE_ADDRESS string

// global var to hold a reference to the ETH RPC Client
var ethRPC *ethrpc.EthRPC

// mapping of addresses to public keys
var ADDRESS_BOOK = make(map[string]string)
var REVERSE_ADDR_BOOK = make(map[string]string)

// Struct to hold data parsed from QuantBCNode JSON response
type BlockChain struct {
    Chain []struct {
        Index        int     `json:"index"`
        MerkleRoot   string  `json:"merkle_root"`
        PreviousHash string  `json:"previous_hash"`
        Proof        string  `json:"proof"`
        Timestamp    float64 `json:"timestamp"`
        Transactions []struct {
            Amount    string `json:"amount"`
            Recipient string `json:"recipient"`
            Sender    string `json:"sender"`
            TxID      string `json:"tx_id"`
        } `json:"transactions"`
    } `json:"chain"`
    Length int `json:"length"`
}

// struct to model simple transaction incoming from SGBs
type SGBTransaction struct {
    From    string  `json:"from"`
    To      string  `json:"to"`
    Amount  string  `json:"amount"`
    Comment string  `json:"comment"`
}

// function to validate incoming SGB Tx
func (ntx SGBTransaction)isValid() bool {
    return ntx.From != "" && ntx.To != "" && ntx.Amount != "" && ntx.Comment != ""
}

// func to get Ethereum TX info by TX-Hash reference
func getETHTxByHash(w http.ResponseWriter, r *http.Request) {
    logRequest(r)

    // enforce JSON encoding on incoming requests
    if r.Header.Get("Content-type") != "application/json" {
        ReplyWithJSON(w, map[string]string{"error": "Payload required in JSON format!"}, http.StatusUnsupportedMediaType)
        return
    }

    // Read body of the request into byte array [b]
    b, err := ioutil.ReadAll(r.Body)
    defer r.Body.Close()
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // unMarshal byte array body from JSON into KeyData struct
    var hashTX map[string]string
    err = json.Unmarshal(b, &hashTX)
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    TX, err := ethRPC.EthGetTransactionByHash(hashTX["hash"])
    if err != nil {
        log.Fatal(err)
    }
    // log.Println(txid)

    if TX.BlockNumber == nil {
        ReplyWithJSON(w, map[string]string{"status":"still pending"}, http.StatusOK)
    } else {
        ReplyWithJSON(w, TX, http.StatusOK)
    }
}

// Handle incoming HTTP POST to '/new_tx' used by SGB Clients to send TX-es
func SGBTransactionHandler(w http.ResponseWriter, r *http.Request) {
    // send incoming request log to GW console
    logRequest(r)

    // limit to POST only and enforce json encoding
    if r.Method != "POST" {
        w.Header().Add("Access-Control-Allow-Methods", "POST")
        ReplyWithJSON(w, map[string]string{"error": "Only HTTP POST is supported!"}, http.StatusMethodNotAllowed)
        return
    }
    // enforce JSON encoding on incoming requests
    if r.Header.Get("Content-type") != "application/json" {
        ReplyWithJSON(w, map[string]string{"error": "Payload required in JSON format!"}, http.StatusUnsupportedMediaType)
        return
    }

    // Read body of the request into byte array [b]
    b, err := ioutil.ReadAll(r.Body)
    defer r.Body.Close()
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    // unMarshal byte array body from JSON into KeyData struct
    var sgbTX SGBTransaction
    err = json.Unmarshal(b, &sgbTX)
    if err != nil {
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    if sgbTX.isValid() == false {
        ReplyWithJSON(w, map[string]string{"error": "Invalid request, missing or incomplete fields"}, http.StatusBadRequest)
        return
    }

    TXHash, err := ethRPC.EthSendTransaction(ethrpc.T{
        // SWAPPED once due to gas run out
        From:  ADDRESS_BOOK["WMO-2"],
        To:    ADDRESS_BOOK["REC-2"],
        Value: big.NewInt(1000000000000000000), // 1 ether = 10^18 wei
    })
    if err != nil {
        log.Println(err)
        ReplyWithJSON(w, map[string]string{"error": err.Error()}, 500)
        return
    }

    go syncToBAL(TXHash, sgbTX.Comment)

    ReplyWithJSON(w, map[string]string{"status": "TX received and processed", "TX-Hash":TXHash}, http.StatusOK)
}

// struct that models TX format used by Quant BCNodes 
type QNetTransaction struct {
    TxID        string `json:"tx_id"`
    Sender      string `json:"sender"`
    Recipient   string `json:"recipient"`
    Amount      string `json:"amount"`
}

// this function takes an Ethereum TX hash and comment string
// and periodically checks with Ethereum Private Network until
// the TX referenced by the string is confirmed, at that point
// it will send a corresponding TX to the Qnet BCNodes with comment 
func syncToBAL(hash string, comment string) {

    for {
        // query TX referenced by HASH given to this func
        TX, err := ethRPC.EthGetTransactionByHash(hash)
        if err != nil {
            log.Println(err)
            continue
        }
        // if the Blocknumber is nill it means TX is still pending, sleep 1 sec and try again 
        if TX.BlockNumber == nil {
            log.Printf("Mining still in progress for TX with hash: %s\n", hash[:10]+"..."+hash[56:])
            time.Sleep(1*time.Second)
        // otherwise it's confirmed and it can be send to Qnet BCNode
        } else {
            log.Printf("Mining done for TX with hash:  %s\n", hash[:10]+"..."+hash[56:])
            data := QNetTransaction{
                TxID:         comment, // needed to tag the transactions
                Sender:       REVERSE_ADDR_BOOK[TX.From], // get valid ETH account addresses
                Recipient:    REVERSE_ADDR_BOOK[TX.To], // get valid ETH account addresses
                Amount:       TX.Value.String(), // amount is same as in Ethereum
            }
            sendTXToQnetBCNode(data, QNET_NODE_ADDRESS) 
            return
        }
    }

}

// function to send HTTP POST request to Quant BCNodes
func sendTXToQnetBCNode(data QNetTransaction, target string) {

    // general HTTP client used for submitting the HTTP POST request
    httpClient := &http.Client{}

    // prepare the JSON body as byte array
    postBody := new(bytes.Buffer)
    json.NewEncoder(postBody).Encode(data)

    // prepare HTTP POST request to /upload API endpoint of keystore
    httpReq, _ := http.NewRequest("POST", fmt.Sprintf("http://%s/transactions/new", target), postBody)
    httpReq.Header.Set("Content-Type", "application/json")
    httpResp, err := httpClient.Do(httpReq)
    if err != nil {
        log.Println(err)
        return
    }
    defer httpResp.Body.Close()
        
    // print log message with HTTP Status Code
    log.Printf("HTTP POST to Quant-BCNode sent, Responsecode: %v\n", target, httpResp.Status)

}

// Simple Handler to test RPC and see if the Ethereum node is mining or not
func IsMiningHandler(w http.ResponseWriter, r *http.Request) {
    logRequest(r)

    // call the ETH RPC and check the version
    eth_is_mining, err := ethRPC.EthMining()
    if err != nil {
        log.Fatal(err)
    }
    miningStr := "false"
    if eth_is_mining == true {miningStr = "true"}

    // write the result back to the client
    w.Write([]byte(miningStr+"\n\r"))
}

// Helper function to display short log message about incoming request method, URL and address
func logRequest(r *http.Request) {
    log.Println("Incoming HTTP " + string(r.Method) + " to \""+r.URL.Path+"\" from " + string(r.RemoteAddr))
}

// Helper function to send JSON reply and HTTP Status Code easily
func ReplyWithJSON(w http.ResponseWriter, msg interface{}, code int) {
    // log.Printf("HTTP Response was: %d\n", code)
    jsonResp, _ := json.MarshalIndent(msg, "", " ")
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(code)
    w.Write(jsonResp)
}

// parse ARGS and set Global variables
func init() {

    flag.StringVar(&GW_OP_MODE, "mode", "non-sync", "Defines if GW does syncing from Qnet or not...")
    flag.StringVar(&ETH_NODE_ADDRESS, "eth", "", "Defines Ethereum RPC engpoing IP and PORT")
    flag.StringVar(&QNET_NODE_ADDRESS, "qnet", "", "Defines Quant BC Node IP and PORT")
    flag.Parse()

    // validate all CLI arg values, format should be "x.x.x.x:xxxx"
    for _, val := range []string{ETH_NODE_ADDRESS, QNET_NODE_ADDRESS} {

        arg := strings.Split(val, ":")
        octets := strings.Split(arg[0], ".")
        if  (len(arg) != 2)  || (len(octets) != 4) {
            log.Println("ERROR: Invalid argument format! \n\n\tUSAGE: ./ethgw -mode=[sync/non-sync] -eth=IP:PORT -qnet=IP:PORT\n")
            os.Exit(1)
        }

        for _, v := range(octets) {
            if len(v) == 0 {
                log.Println("ERROR: Invalid IP Address! \n\n\tUSAGE: ./ethgw -mode=[sync/non-sync] -eth=IP:PORT -qnet=IP:PORT\n")
                os.Exit(2)
            }
        }

        port := arg[1]
        _, err := strconv.Atoi(port)
        if err != nil {
            log.Println("ERROR: Invalid PORT number! \n\n\tUSAGE: ./ethgw  -mode=[sync/non-sync] -eth=IP:PORT -qnet=IP:PORT\n")
            os.Exit(3)
        }
    }
    // only needed in "non-sync" mode when the GW accepts TX-es from SGB-s
    API_PORT = "8888"
    // helps with the use of Ethereum Account addresses by mapping them to Strings in a MAP
    ADDRESS_BOOK = map[string]string{
        "REC-1":    "0xcc926f018f6f1052987e732c8250bbb7a73d83a3",
        "WMO-1":    "0x3d0c46847b3a6ad1eb1f7409c83acd63f60454db",
        "CITY-1":   "0x2a62d902b235f8557d97c4eaefffcb5bf14be75a",
        "REC-2":    "0x6feb1d0347fbb1c3c9cb574380b5ba93666cf6f3",
        "WMO-2":    "0x936468a25face673e4ae6bd928fb74611ae9b959",
        "CITY-2":   "0x7c9849442b0c30ec530ee4957f4600abe913c962",
    }
    // helps with the use of Ethereum Account addresses by mapping them SGB domain UUIDs via a MAP
    REVERSE_ADDR_BOOK = map[string]string{
        "0xcc926f018f6f1052987e732c8250bbb7a73d83a3": "f400dd0660fe11e996e71b58cb63beda",//REC-1
        "0x3d0c46847b3a6ad1eb1f7409c83acd63f60454db": "f530b7be60fe11e9994017619f036867",//WMO-1
        "0x2a62d902b235f8557d97c4eaefffcb5bf14be75a": "f570f9c860fe11e9a4b5c36f018d68c3",//CITY-1
        "0x6feb1d0347fbb1c3c9cb574380b5ba93666cf6f3": "f400dd0660fe11e996e71b58cb63beda",//REC-2
        "0x936468a25face673e4ae6bd928fb74611ae9b959": "f530b7be60fe11e9994017619f036867",//WMO-2
        "0x7c9849442b0c30ec530ee4957f4600abe913c962": "f570f9c860fe11e9a4b5c36f018d68c3",//CITY-2
    }
}

// gets the most recent BC from the QNet BCNode to use for syncing
func fetchQnetBlockchain(address string) (BlockChain, error) {
    // send the HTTP Get to the node
    resp, err := http.Get("http://" + address + "/chain")
    if err != nil {
        return BlockChain{}, err
    }
    // read out the response body
    data, err := ioutil.ReadAll(resp.Body)
    resp.Body.Close()
    if err != nil {
        return BlockChain{}, err
    }
    // marshal the JSON body into a struct
    var bc BlockChain
    err = json.Unmarshal(data, &bc)
    if err != nil {
        return BlockChain{}, err
    }
    // return the struct to the calling func
    return bc, nil
}

// this is just a quick and dirty way of reading an Integer from DISK which signifies
// until when the Qnet BC is processed and synced, to not start from the 1st block always
func readLength() int {
    readIn, err := ioutil.ReadFile("/var/tmp/eth-scenario/logs/eth-gw1-length.txt")
    if err != nil {
        log.Printf("ERR: %v", Green("File Not Found"))
        return 0
    }
    num, err := strconv.Atoi(strings.TrimSuffix(string(readIn), "\n"))
    if err != nil {
        log.Printf("ERR: %v", Green("File cannot be parsed"))
        return 0
    } 
    return num
}

// quick and dirty way to store in simple TXT file a number which represents the point until which the Qnet BC is synced
func saveLength(length int) {
    err := ioutil.WriteFile("/var/tmp/eth-scenario/logs/eth-gw1-length.txt", []byte(strconv.Itoa(length)), 0644)
    if err != nil {
        log.Printf("Error: %v\n", Red(err))
    }
}

// convenience function to trigger consensus on the Qnet BCNodes
func triggerQuantConsensus() {
    resp, err := http.Get("http://10.0.0.100:5000/nodes/resolve")
    if err != nil {
        log.Printf("Error calling consens manually: %v", Red(err))
    }
    body, err := ioutil.ReadAll(resp.Body)
    log.Printf("Response from Qnet Consensus call:\n%v", string(body))
    resp.Body.Close()
}


// func that syncs the Qnet BC to the ethereum private BC (1)
func synTXfromQnet() {

    // load from txt file a number which represents the height until which the processing / syncing is DONEs
    processedLength := readLength()

    // counter to periodically trigger consensus on the two Qnet BCNodes
    var counter int = 0

    // infinite loop in which BCNode API is queried for the BC and is syned to Ethereum network 
    for {

        // quick and easy way of triggering consensus of the two QUANT nodes every 50 seconds
        counter ++
        if counter == 10 {
            triggerQuantConsensus()
            counter = 0
        }

        time.Sleep(5*time.Second)
        bc, err := fetchQnetBlockchain(QNET_NODE_ADDRESS)
        if err != nil {
            log.Println(err)
            continue
        }

        // do some validations on the BC that is fetched...
        if bc.Length <2 {
            log.Println("Chain too short. Let it growww!")
            continue
        }
        if processedLength == bc.Length {
            log.Println("Already cauht up with syncing!")
            continue
        }

        // if the processedLength is for some reason longer than the BC Length then it's
        // assumed that the TXT file from which it was read is outdated and it needs to be reset
        if processedLength > bc.Length {
            processedLength = 0
            saveLength(processedLength)
            continue
        }

        // loop through the BC chain data and take TX-es from each Block and submit them all in parallel
        for i, blck := range bc.Chain {
            // skip until the current block reaches the last synced height
            if blck.Index <= processedLength {
                continue
            }

            // create a waitgroup for parallel submissions via goroutines
            var waitgroup sync.WaitGroup
            for j, tx := range  blck.Transactions {

                // skip TX-es that are called genesis or mining-reward
                if (tx.TxID != "mining-reward" && tx.TxID != "genesis") {
                    log.Printf("blck: %v, tx: %v TX-ID: %v\n", Yellow(i), Yellow(j), Yellow(tx.TxID))
                    
                    // increment waitgroup counter and start parallel go routine
                    // where the TX is submitted and confirmed
                    waitgroup.Add(1)
                    go func() {
                        TXHash, err := ethRPC.EthSendTransaction(ethrpc.T{
                            From:  ADDRESS_BOOK["WMO-1"],
                            To:    ADDRESS_BOOK["REC-1"],
                            Value: big.NewInt(1000000000000000000), // 1 ether = 10^18 wei
                        })
                        if err != nil {
                            log.Println(err)
                            waitgroup.Done()
                        }
                        // before exiting the go-routine make sure the submitted TX is confirmed
                        for {
                            TX, err := ethRPC.EthGetTransactionByHash(TXHash)
                            if err != nil {
                                log.Println(err)
                                continue
                            }
                            if TX.BlockNumber == nil {
                                log.Printf("Confirmation pending for TX with hash: %s\n", Red(TXHash[:10]+"..."+TXHash[56:]))
                                time.Sleep(2*time.Second)
                            } else {
                                log.Printf("Mining done for submitted TX with hash: %s\n", Green(TXHash[:10]+"..."+TXHash[56:]))
                                break                                
                            }
                        }
                        // at this point the TX is assumed ot be confirmed so the WG counter can be decremented
                        waitgroup.Done()
                    }()
                }
            }
            // block here until all parallel goroutines finished syncing TXes from the current BLOCK 
            waitgroup.Wait()
            // increment coutner signalling the height which is synced
            processedLength++
            // save the counter to make it resistant to restarts
            saveLength(processedLength)
        }
    }
}


func main() {

    // create ETH RPC engpoint
    ethRPC = ethrpc.New("http://" + ETH_NODE_ADDRESS)

    // check which flag was passed to the program at startup
    //   -mode="non-sync" means that the GW takes transactions from SGBS (gw2 on topology)
    //   -mode="sync"     means that the GW synchronises BC from Qnet to Ethereum (gw1 on topology)
    switch GW_OP_MODE {
    case "non-sync":    
        http.HandleFunc("/is_mining", IsMiningHandler)
        http.HandleFunc("/new_tx", SGBTransactionHandler)
        http.HandleFunc("/tx_by_hash", getETHTxByHash)
        log.Printf("ETH GW is running in NON-SYNC mode!")
        log.Printf("Ethereum RPC target endpoint: %s\n", Green(ETH_NODE_ADDRESS))
        log.Printf("Local API endpoint for digesting SGB TX-es:%s\n", Green(API_PORT))
        log.Fatal(http.ListenAndServe(":"+API_PORT, nil))
    case "sync":
        log.Printf("Eth GW running in SYNC mode\n")
        log.Println("Sleep for", Yellow("5 sec"), "to let nodes start up...")
        time.Sleep(5*time.Second)
        synTXfromQnet()
    }
}