#!/bin/env python
import sys
import time
import requests
from functools import partial

from mininet.topo import MinimalTopo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.node import Host, CPULimitedHost, RemoteController
from mininet.util import specialClass
from mininet.log import (lg, info, error)

from SWMTopos import SWMTopoPoW, SWMTopoQuant, SWMTopoQuantumEthereum


class TestScenarioV1(object):
    """ Class definition for Test Scenario V1: 
    -> PoW BC on servers and SGBClient on SGB Nodes """
    def __init__(self, topo, ctrllr_ip='127.0.0.1', ctrllr_port=6653): 
        self.topo = topo
        self.ctrllr_ip = ctrllr_ip
        self.ctrllr_port = ctrllr_port


    def run( self ):
        """ Instantiates the Mininet network and 
        runs appropriate function on each node """ 
        net = Mininet(topo=self.topo, 
                      controller=partial(RemoteController, 
                                         ip=self.ctrllr_ip, 
                                         port=self.ctrllr_port))
        net.start()

        for node in net.hosts:
            if 'srv' in node.name:
                node.start()
            if node.name in ['sgb1', 'sgb2', 'sgb3']:
                node.start(target_address='10.0.0.100')
            if node.name in ['sgb4', 'sgb5', 'sgb6']:
                node.start(target_address='10.0.0.101')
            if node.name in ['sgb7', 'sgb8', 'sgb9']:
                node.start(target_address='10.0.0.102')
            if node.name in ['sgb10', 'sgb11', 'sgb12']:
                node.start(target_address='10.0.0.103')
        # start the Mininet CLI
        result=CLI(net)
        # after CLI exits shut down the hosts properly
        for node in net.hosts:
            node.stop()
        # finally, shut the whole network down
        net.stop()

class TestScenarioV2(object):
    """ Class definition for Test Scenario V1: 
    -> Quant BC on servers and SGBClient on SGB Nodes + QKD Emu + Keyworkers """
    def __init__(self, topo, ctrllr_ip='127.0.0.1', ctrllr_port=6653): 
        self.topo = topo
        self.ctrllr_ip = ctrllr_ip
        self.ctrllr_port = ctrllr_port


    def run( self ):
        """ Instantiates the Mininet network and 
        runs appropriate function on each node """ 
        
        net = Mininet(topo=self.topo, 
                      controller=partial(RemoteController, 
                                         ip=self.ctrllr_ip, 
                                         port=self.ctrllr_port))
        net.start()

        for node in net.hosts:
            if 'srv' in node.name:
                node.start()
                node.kwstart()
            if node.name in ['sgb1',  'sgb2',  'sgb3']:
                node.start(target_address='10.0.0.100')
            if node.name in ['sgb4',  'sgb5',  'sgb6']:
                node.start(target_address='10.0.0.101')
            if node.name in ['sgb7',  'sgb8',  'sgb9']:
                node.start(target_address='10.0.0.102')
            if node.name in ['sgb10', 'sgb11', 'sgb12']:
                node.start(target_address='10.0.0.103')
            if node.name == 'qkde':
                # start the QKDE program with the keyworkers on each BCNode passed as CLI argument
                # node.start(['10.0.0.100:55554', '10.0.0.101:55554', '10.0.0.102:55554', '10.0.0.103:55554'])
                node.start_qkd_emulators(['10.0.0.100:55554', '10.0.0.101:55554', '10.0.0.102:55554', '10.0.0.103:55554'])

        result=CLI(net)
        
        for node in net.hosts:
            node.stop()
            if 'srv' in node.name:
                node.kwstop()

        net.stop()


class TestScenarioV3(object):
    """ Class definition for Test Scenario V3.
    > Scenario involving Quant BC + private Ethereum networks + API gws synchronising between them"""
    def __init__(self, topo, ctrllr_ip='127.0.0.1', ctrllr_port=6653): 
        self.topo = topo
        self.ctrllr_ip = ctrllr_ip
        self.ctrllr_port = ctrllr_port


    def run( self ):
        """ Instantiates the Mininet network and runs appropriate function on each node """ 
        net = Mininet(topo=self.topo, 
                      controller=partial(RemoteController, ip=self.ctrllr_ip, port=self.ctrllr_port))
        
        # Initialize the network nodes
        net.start()

        # Start applications on different nodes according to their specification
        for node in net.hosts:
            if 'srv' in node.name:
                node.start()
                node.kwstart()
            if node.name in ['sgb1',  'sgb2']: #,  'sgb3']:
                node.change_cout_dir_to("/var/tmp/eth-scenario/logs/")
                node.start(target_address='10.0.0.69', is_ether_scenario=True)
            if node.name == 'qkde1':
                node.start("100-101", ['10.0.0.100:55554', '10.0.0.101:55554'])
                node.change_cout_dir_to("/var/tmp/eth-scenario/logs/", "100-101")
            if node.name == 'eth21':
                node.update_srv_working_dir("/var/tmp/eth-scenario/priv_eth_2/e2-1")
                node.configure_main_node("10.0.0.61")
                node.start()
            if node.name == 'eth22':
                node.update_srv_working_dir("/var/tmp/eth-scenario/priv_eth_2/e2-2")
                node.start()
            if node.name == 'eth11':
                node.update_srv_working_dir("/var/tmp/eth-scenario/priv_eth_1/e1-1")
                node.configure_main_node("10.0.0.51")
                node.start()
            if node.name == 'eth12':
                node.update_srv_working_dir("/var/tmp/eth-scenario/priv_eth_1/e1-2")
                node.start()
            if node.name == 'gw2':
                # supply the ETH RPC and QNET API targets to this GW node
                node.start("non-sync", "10.0.0.61:8080", "10.0.0.101:5000")
            if node.name == 'gw1':
                # supply the ETH RPC and QNET API targets to this GW node
                node.start("sync", "10.0.0.51:8080", "10.0.0.100:5000")
            if node.name == 'op':
                node.start()
                
        # get the CLI to the initialized network
        result = CLI(net)
        # stop applications before stopping network
        for node in net.hosts:
            node.stop()
            if 'srv' in node.name:
                node.kwstop()
        # Finish simulation
        net.stop()



if __name__ == '__main__':

    # Set log level for Mininet
    lg.setLogLevel( 'info' )

    # Check if ARG is passed correctly
    if len(sys.argv) != 2:
        print("ERROR: Invalid argument!\nUSAGE: 'sugo python runTestNet.py [pow/quant/ether]'"    )
        sys.exit()

    # Determine the topology to be used in this run
    if sys.argv[1] == "pow":
        test_topo = SWMTopoPoW()
        test = TestScenarioV1(topo=test_topo, ctrllr_ip='127.0.0.1', ctrllr_port=6653)
        test.run() 
    elif sys.argv[1] == "quant":
        test_topo = SWMTopoQuant()
        test = TestScenarioV2(topo=test_topo, ctrllr_ip='127.0.0.1', ctrllr_port=6653)
        test.run() 
    elif sys.argv[1] == "ether":
        test_topo = SWMTopoQuantumEthereum()
        test = TestScenarioV3(topo=test_topo, ctrllr_ip='127.0.0.1', ctrllr_port=6653)
        test.run() 
    else:
        print("ERROR: Invalid argument!\nUSAGE: 'sugo python runTestNet.py [pow/quant/ether]'")
        sys.exit()