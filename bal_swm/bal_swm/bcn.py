#!/usr/bin/env python

from flask import Flask, jsonify, request
from subprocess import check_output

import json
import logging
import requests

import datetime
import time
import threading
import os

# Instantiate the Node
from bctypes import QuantumBlockChain, POWBlockChain

app = Flask(__name__)
app.config.update(
    JSONIFY_PRETTYPRINT_REGULAR=True
)

# Instantiate the Blockchain
blockchain = None


@app.route('/chain/valid', methods=['GET'])
def is_chain_valid():
    return jsonify(blockchain.is_chain_valid()), 200

@app.route('/mine', methods=['GET'])
def do_mine():
    return blockchain.mine()

@app.route('/transactions/new', methods=['POST'])
def handle_new_transaction():

    # get values from HTTP REST body
    values = request.get_json()
    
    # validate fields
    required = ['sender', 'recipient', 'amount', 'tx_id', 'status', 'timestamp']
    if not all(k in values for k in required):
        return 'Missing values', 400

    if values['status'] == "new":
        blockchain.forward_to_peers(values['tx_id'],
                                    values['sender'],
                                    values['recipient'],
                                    values['amount'],
                                    values['timestamp'])

    ## check if mining is running and add incoming TX into other pool.
    if blockchain.pow_mining_running:
        # add new TX to list of additional TX-es that cannot make it to current block
        blockchain.extra_transactions.append({
            'tx_id': values['tx_id'],
            'sender': values['sender'],
            'recipient': values['recipient'],
            'amount': values['amount'], 
            'timestamp': values['timestamp']
        })
        # send back confirmation that TX was saved for later block
        response = {
            'status': 'Mining in progress, Your TX will be saved for a later block'
        }
        return jsonify(response), 201
    else:
        if len(blockchain.current_transactions) >= 19:
            blockchain.mine()
            # move any extra TXes back to main TX list
            blockchain.move_extras_to_main_tx_list()
        # Create a new Transaction
        index = blockchain.new_transaction( values['tx_id'],
                                            values['sender'],
                                            values['recipient'],
                                            values['amount'], 
                                            values['timestamp'])
        response = {
            'message': 'Transaction will be added to Block {} and forwarded to PEERS!'.format(index),
            'outstanding_transaction_count': len(blockchain.current_transactions)
        }
        return jsonify(response), 201


@app.route('/chain', methods=['GET'])
def do_full_chain():
    return jsonify(blockchain.full_chain()), 200

@app.route('/chain/length', methods=['GET'])
def do_return_chain_length():
    return jsonify({ "ip_address":blockchain.ip_address, "length":len(blockchain.chain)}), 200


@app.route('/nodes', methods=['GET'])
def do_get_nodes():
    return blockchain.get_nodes()


@app.route('/nodes', methods=['DELETE'])
def do_remove_nodes():
    blockchain.remove_nodes()
    response = {
        'message': 'Nodes removed',
        'total_nodes': list(blockchain.nodes),
    }
    return jsonify(response)


@app.route('/nodes/register', methods=['POST'])
def do_register_nodes():
    values = request.get_json()
    return blockchain.register_nodes(values)


@app.route('/nodes/resolve', methods=['GET'])
def do_consensus():
    return blockchain.consensus()


@app.route('/info', methods=['GET'])
def return_node_info():
    if blockchain.ip_address == "10.0.0.100":
        srvname = "srv1"
    elif blockchain.ip_address == "10.0.0.101":
        srvname = "srv2"
    elif blockchain.ip_address == "10.0.0.102":
        srvname = "srv3"
    else:
        srvname = "srv4"
    resp = {
        "status": "alive",
        "name": srvname, 
        "node_id": blockchain.node_identifier
    }
    return jsonify(resp), 200


# Helper class to implement background periodic task via threading
class BackgroundNodeDiscovery(object):
    def __init__(self, interval=30):
        self.interval = interval
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        thread.start()

    # Helper function to create the channel ID from two given IP addresses in format 'X-X'
    def determine_quantum_channel_id(self, firstIP, secondIP):
        firstIPLastOctet = firstIP.split(".")[3]
        secondIPLastOctet = secondIP.split(".")[3]
        if int(firstIPLastOctet) < int(secondIPLastOctet):
            return firstIPLastOctet + "-" + secondIPLastOctet
        return secondIPLastOctet + "-" + firstIPLastOctet

    # Background task that runs on each BCNode that periodically checks alive nodes and registers them
    def run(self):
        own_ip = check_output(['hostname', '--all-ip-addresses']).strip('\n').strip(' ')

        while True:
            # More statements comes here
            alive_nodes = []

            # loop through servers and ping them then construct channel ID if alive
            for server_ip in ["10.0.0.%s" % (h) for h in range(100, 110)]:
                if server_ip != own_ip:
                    if os.system("timeout 0.1 ping -c1 {} >/dev/null 2>&1".format(server_ip)) == 0:
                        alive_nodes.append({
                            "address": "http://"+server_ip+":5000",
                            "channel_id": self.determine_quantum_channel_id(own_ip, server_ip)
                        })
            if len(alive_nodes) == 0:
                r = requests.delete("http://" + own_ip + ":5000/nodes", json=None)
            else:
                nodes = {
                    "nodes": alive_nodes
                }
                r = requests.post("http://" + own_ip + ":5000/nodes/register", json=nodes)
            time.sleep(self.interval)


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    parser.add_argument('-d', '--db', default='', help='db file')
    parser.add_argument('-n', '--name', default='', help='node identifier name (UUID)')
    parser.add_argument('-v', '--variant', default='pow', help='variant of blockchain "pow[:difficulty]" or "quant"')
    
    args = parser.parse_args()
    port = args.port
    dbfile = args.db
    node_id = args.name
    
    if args.variant.find('pow') == 0:
        pow = args.variant.split(':')
        if len(pow) == 2:
            blockchain = POWBlockChain(app, node_id, difficulty=int(pow[1]))
        else:
            blockchain = POWBlockChain(app, node_id)
    elif args.variant == 'quant':
        blockchain = QuantumBlockChain(app, node_id)
        sq = BackgroundNodeDiscovery()
    else:
        blockchain = POWBlockChain(app, node_id)
    if args.db:
        print("DB: " + dbfile)
        blockchain.init_db(dbfile)

    app.run(host='0.0.0.0', port=port, threaded=True, debug=True)