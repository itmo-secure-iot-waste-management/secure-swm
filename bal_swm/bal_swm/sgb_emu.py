#!/usr/bin/env python
from random import randint
from time import sleep, time
import sys
import requests


class SGBEmulator(object):

    def __init__(self, sgb_name, sgb_target):
        self.identity_store = {
                "sgb1" : "6a0a0429545c434ebe63e5aa390f1bb7",
                "sgb2" : "94ee7528441b441e8e6212ac0f951456",
                "sgb3" : "c692accf77d34257ba019bc57981a3ca",
                "sgb4" : "20013eb8549844f5ad3fad4860bb8ddc",
                "sgb5" : "45ac357cb86d46ee9e89fdbd108bf320",
                "sgb6" : "d28af72201a1430894fa87350ae4449f",
                "sgb7" : "283dc93202e34f52b2f3924f48c60033",
                "sgb8" : "bb49dc620dde4e69b34b9e08757963d8",
                "sgb9" : "74b20033a1ac4ea7b7694cbc51491fa1",
                "sgb10" : "b8e54f524fd511e9b514a3f6393a15da",
                "sgb11" : "b9906dce4fd511e9a93e27ba6b6a5ed1",
                "sgb12" : "ba63e7084fd511e9a6bebfbd88b69c2b",
                "srv1" : "60c42574f5ca4f3b9057207b7f332a68",
                "srv2" : "076584247dc041ad9182f58255b31c09",
                "srv3" : "a655c3ab2e6e48d49d18ae34ba6bcc25",
                "srv4" : "52ea9e5e4fd111e99eb9734ce850821e"
        }
        self.sgb_name = sgb_name
        self.sgb_target = sgb_target

    def start_sgb_emulation(self):
        counter = 0
        while True:
            data = {
                "timestamp": int(time()),
                "status": "new",
                "tx_id": "{}-{}".format(self.sgb_name, counter),
                "sender": self.identity_store[self.sgb_name],
                "recipient": self.identity_store["sgb" + str(randint(1,9))],
                "amount":randint(5,20)
            }
            r = requests.post('http://{}:5000/transactions/new'.format(self.sgb_target), json = data)
            sleep(randint(2,4))
            counter += 1

    def start_sgb_emulation_etherscenario(self):
        counter = 0
        while True:
            data = {
                "timestamp": int(time()),
                "status": "new",
                "from": "f400dd0660fe11e996e71b58cb63beda",
                "to": "Af530b7be60fe11e9994017619f036867",
                "amount": "10000",
                "comment": "{}-{}".format(self.sgb_name, counter),
            }
            r = requests.post('http://{}:8888/new_tx'.format(self.sgb_target), json = data)
            sleep(randint(3,6))
            counter += 1



if __name__ == '__main__':

    if len(sys.argv) < 2 or len(sys.argv) > 4:
        print("EXAMPLE USAGE: 'python sgbe.py [sgb1] [10.0.0.100] [eth]'")
        sys.exit(1)

    print('Initiating SGB Emulator routine in 10 seconds... (ETH NEEDS TIME)')
    sleep(10)

    name = sys.argv[1]
    target = sys.argv[2]

    sgb = SGBEmulator(sgb_name=name, sgb_target=target)

    if len(sys.argv) == 4 and sys.argv[3] == "eth":
        print("starting eth version")
        sgb.start_sgb_emulation_etherscenario()
    else:
        print("starting normal version")
        sgb.start_sgb_emulation()