import requests
# import statiscdtics

import numpy as np
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import sys



nodes = ["10.0.0.100:5000"] #, "10.0.0.101:5000", "10.0.0.102:5000", "10.0.0.103:5000"]

for node in nodes:
	r = requests.get("http://{}/chain".format(node))

	bc = r.json()
	elapsed = []

	for block in bc['chain']:
		blocktime = block['timestamp']
		# print("Block Index: {}, timestamp: {}".format(block['index'], blocktime))
		for tx in block['transactions']:
			if tx['tx_id'] in ["genesis", "mining-reward"]:
				continue
			# print("Elapsed time for TX({})> {}-{}={}".format(tx['tx_id'], blocktime, tx['timestamp'], blocktime-tx['timestamp']))
			elapsed.append(blocktime-tx['timestamp'])
			

	print("\nNODE: {}\nAverage: {}\nSTD {}\nPercentile: {}\nMax: {}\n".format(node,
																	  np.average(elapsed), 
																	  np.std(elapsed),
																	  np.percentile(elapsed,90),
																	  np.max(elapsed)))

	txcounter = 0
	start = 0
	end = 0
	for block in bc['chain']:
		if block['index'] == 1:
			start = block['timestamp']
		for tx in block['transactions']:
			txcounter+=1
		if block['index'] == len(bc['chain']):
			end = block['timestamp']

	print("Start: {}\nEnd {}\nElapsed time {}sec\nTX-Count {}\nTX/sec {}\n".format(start, end, end-start, txcounter, float(txcounter)/float((end-start))))


df=pd.DataFrame({'x': range(0,len(elapsed)), 
				 'y1': elapsed,
				 'y2': np.average(elapsed),
				 'y3': np.percentile(elapsed, 90)})


plt.plot( 'x', 'y1', data=df, marker='', markerfacecolor='blue', markersize=8, color='skyblue', linewidth=1, label="actual TX delay")
plt.plot( 'x', 'y2', data=df, marker='', color='olive', linewidth=2, label="average TX delay")
plt.plot( 'x', 'y3', data=df, marker='', color='red', linewidth=2, linestyle='dashed', label="90th percentile")
# plt.text(-200,9, "comment")
plt.title(sys.argv[1])

#plt.savefig('test.eps', format='eps', dpi=900)
# plt.savefig("pow.png", format="png")

plt.legend()
plt.show()