# OPERATORS mininet node implementation details

This program is written for Scenario V3 to enable easy supervision of the Ethereum balances in the PrivNet1 where operators keep track of their balances.

### Basic functionalities:
- polls the ETH RPC via JSON and gets current account balances every X seconds
- saves account balance data to a local database
- plots last 100 or so values for each account in a PNG file in current directory

If database file does not exist in current working directory it will create it from scratch with correct schema.
ETH RPC address is hardcoded for now, as there is no reason it would change anyway...