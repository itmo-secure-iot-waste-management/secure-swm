package main 

import (
    "log"
    "crypto/rand"
    "strings"
    "strconv"
    "net/http"
    "time"
    "fmt"
    "os"
    "bytes"
    "encoding/json"
)

// instance ID for this QKD Emulator that handles 
var channelID string

// Data structure to help with JSON Encoding/Decoding
type KeyData struct {
    Channel       string      `json:"channel"`
    Key           []byte      `json:"key"`
}

// Helper function that will generate a new 256 bit key for the QKD Emulator
func generate256BitKey() []byte {
    b := make([]byte, 32)
    _, err := rand.Read(b)
    if err != nil {
        return nil
    }
    return b
}

// function to send HTTP POST request
func postKey(kd KeyData, target string) {

    // general HTTP client used for submitting the HTTP POST request
    httpClient := &http.Client{}

    // prepare the JSON body as byte array
    postBody := new(bytes.Buffer)
    json.NewEncoder(postBody).Encode(kd)

    // prepare HTTP POST request to /upload API endpoint of keystore
    httpReq, _ := http.NewRequest("POST", fmt.Sprintf("http://%s/upload", target), postBody)
    httpReq.Header.Set("Content-Type", "application/json")

    // execute HTTP POST request
    httpResp, err := httpClient.Do(httpReq)
    if err != nil {
        log.Println(err)
        return
    }
    defer httpResp.Body.Close()
        
    // print log message with HTTP Status Code
    log.Printf("HTTP POST to Keystore Instance (%v) with result (%v)\n", target, httpResp.Status)

}

// takes a string slice of KW-Pairs and splits them and installs same key in each KW of same pair
func installKeys(targetOne string, targetTwo string) {

    // generate HEX encoded 256 bit random key
    key := generate256BitKey()

    // creating the struct for JSON marshalling
    postData :=  KeyData{
        Channel: channelID,
        Key: key,
    }

    // send HTTP POST to each keystore attached to this QUANTUM channel
    postKey(postData, targetOne)
    postKey(postData, targetTwo)
}

func createChannelID(one string, two string) string {
    return strings.Split(strings.Split(one, ":")[0], ".")[3] + "-" +
           strings.Split(strings.Split(two, ":")[0], ".")[3]
}

func main() {

    fmt.Println()

    // check if program is called properly with CLI arguments
    if (len(os.Args[1:]) != 2) {
        log.Println("ERROR: Missing command line arguments!\n\n\tUSAGE: ./goqkdemu [IP1:PORT] [IP2:PORT]\n")
        os.Exit(1)
    }

    if ( os.Args[1] == os.Args[2] ) {
        log.Println("ERROR: CLI arguments musd differ from one another!\n\n\tUSAGE: ./qkdemu IP1:PORT IP2:PORT\n")
        os.Exit(1)      
    }

    // validate all CLI arg values
    for _, val := range os.Args[1:] {

        arg := strings.Split(val, ":")
        if  (len(arg) != 2)  && (len(strings.Split(arg[0], ".")) != 4) {
            log.Println("ERROR: Invalid argument format! \n\n\tUSAGE: ./qkdemu IP1:PORT IP2:PORT\n")
            os.Exit(1)
        }

        octets := strings.Split(arg[0], ".")
        if len(octets) != 4 {
            log.Println("ERROR: Invalid IP Address! \n\n\tUSAGE: ./qkdemu IP1:PORT IP2:PORT\n")
            os.Exit(2)
        }

        port := arg[1]
        _, err := strconv.Atoi(port)
        if err != nil {
            log.Println("ERROR: Invalid PORT number! \n\n\tUSAGE: ./qkdemu IP1:PORT IP2:PORT\n")
            os.Exit(3)
        }
    }

    channelID = createChannelID(os.Args[1], os.Args[2])

    // loop endlessly an upload new keymaterial to each pair
    log.Printf("QKD Emulation for channel ID %s starting in 1 second...\n", channelID)
    
    time.Sleep(1*time.Second)

    // start endless loop installing new keys every 5 seconds
    for {
        installKeys(os.Args[1], os.Args[2])
        time.Sleep(10*time.Second)
    }
}